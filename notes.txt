Standard deviation

~/Dropbox/bp/code: master *% u+1 python playground.py
file /Users/miksu/Documents/trading/AAPL/all-raw.tsv successfully loaded
AAPL avg is 512.689197778
AAPL std deviation is 32.8775908625
file /Users/miksu/Documents/trading/ADBE/all-raw.tsv successfully loaded
ADBE avg is 53.107061985
ADBE std deviation is 3.99546932923
file /Users/miksu/Documents/trading/AMZN/all-raw.tsv successfully loaded
AMZN avg is 340.540643989
AMZN std deviation is 37.7482563439
file /Users/miksu/Documents/trading/GOOG/all-raw.tsv successfully loaded
GOOG avg is 972.050263135
GOOG std deviation is 88.691258908
file /Users/miksu/Documents/trading/INTC/all-raw.tsv successfully loaded
INTC avg is 23.6617633334
INTC std deviation is 0.975979948732
file /Users/miksu/Documents/trading/MSFT/all-raw.tsv successfully loaded
MSFT avg is 34.9908898524
MSFT std deviation is 2.26181837033
file /Users/miksu/Documents/trading/NFLX/all-raw.tsv successfully loaded
NFLX avg is 326.154029635
NFLX std deviation is 33.3222920068
file /Users/miksu/Documents/trading/ORCL/all-raw.tsv successfully loaded
ORCL avg is 33.7781540872
ORCL std deviation is 1.31091273003
file /Users/miksu/Documents/trading/TSLA/all-raw.tsv successfully loaded
TSLA avg is 156.655319853
TSLA std deviation is 19.7216905175
file /Users/miksu/Documents/trading/YHOO/all-raw.tsv successfully loaded
YHOO avg is 33.8171166452
YHOO std deviation is 3.8920265293


Classes distribution
=============
AAPL
[11000, 11057]
[10180, 1442, 10435]
[9769, 2274, 10014]
[8576, 4686, 8795]
[7530, 6817, 7710] BEST
[6665, 8617, 6775]

file datasets/output-ADBE-train.csv successfully loaded
=============
ADBE
[11542, 9809]
[5662, 9817, 5872] BEST
[2974, 15321, 3056]
[815, 19726, 810]
[195, 20974, 182]
[60, 21232, 59]

file datasets/output-AMZN-train.csv successfully loaded
=============
AMZN
[10752, 10939]
[9843, 1614, 10234]
[9389, 2547, 9755]
[8022, 5261, 8408]
[6809, 7775, 7107] BEST
[5774, 9870, 6047]

file datasets/output-GOOG-train.csv successfully loaded
=============
GOOG
[10229, 10256]
[9719, 875, 9891]
[9467, 1384, 9634]
[8876, 2633, 8976]
[8143, 4106, 8236]
[7540, 5224, 7721] BEST

file datasets/output-INTC-train.csv successfully loaded
=============
INTC
[13071, 8874]
[1220, 19511, 1214] BEST
[298, 21342, 305]
[6, 21928, 11]
[0, 21945, 0]
[0, 21945, 0]

file datasets/output-MSFT-train.csv successfully loaded
=============
MSFT
[12304, 9660]
[3079, 15743, 3142] BEST
[1354, 19183, 1427]
[200, 21567, 197]
[38, 21896, 30]
[5, 21949, 10]

file datasets/output-NFLX-train.csv successfully loaded
=============
NFLX
[10649, 10764]
[9981, 1144, 10288]
[9664, 1773, 9976]
[8696, 3713, 9004]
[7819, 5568, 8026] BEST
[6999, 7179, 7235]

file datasets/output-ORCL-train.csv successfully loaded
=============
ORCL
[12442, 9509]
[2899, 16230, 2822] BEST
[1156, 19768, 1027]
[116, 21756, 79]
[16, 21926, 9]
[5, 21942, 4]

file datasets/output-TSLA-train.csv successfully loaded
=============
TSLA
[11206, 10780]
[10388, 1363, 10235]
[9807, 2517, 9662]
[8444, 5153, 8389]
[7457, 7116, 7413] BEST
[6406, 9245, 6335]

file datasets/output-YHOO-train.csv successfully loaded
=============
YHOO
[12232, 9721]
[5333, 11457, 5163] BEST
[2784, 16515, 2654]
[769, 20463, 721]
[214, 21560, 179]
[107, 21786, 60]



PredictClass3:
==========================
APPLE:		-$0.17, +$0.17        [7137, 7608, 7312]
ADOBE:		-$0.02, +$0.02        [6783, 7546, 7022]
AMAZON:		-$0.15, +$0.15        [6809, 7775, 7107]
GOOGLE:		-$0.29, +$0.29        [6454, 7389, 6642]
INTEL:		-$0.01, +$0.01        [6675, 8607, 6663]
MICROSOFT:	-$0.01, +$0.01        [6876, 8112, 6976]
NETFLIX:	-$0.21, +$0.21        [6866, 7456, 7091]
ORACLE:		-$0.01, +$0.01        [7010, 7911, 7030]
TESLA:		-$0.15, +$0.15        [7499, 7035, 7452]
YAHOO:		-$0.02, +$0.02        [7333, 7352, 7268]


